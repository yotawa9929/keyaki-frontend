import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import * as firebase from "firebase";
import { CONFIG } from "../../configs/config";

@Component({
  selector: "landing-page",
  templateUrl: "LandingPage.html",
})
export class LandingPage {

  constructor(public navCtrl: NavController) {}

  public buttonClicked() {
    const provider = new firebase.auth.FacebookAuthProvider();

    firebase.auth().signInWithPopup(provider).then(
      (result) => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      const token = result.credential.accessToken;
      // The signed-in user info.
      const user = result.user;
      // ...
      console.log("result", result);
    }).catch((error) => {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
      const email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      const credential = error.credential;
      // ...
    });

  }
}
